package com.bartslota.reservations

import com.github.tomakehurst.wiremock.WireMockServer
import io.github.resilience4j.circuitbreaker.CircuitBreaker
import io.github.resilience4j.circuitbreaker.CircuitBreakerRegistry
import org.springframework.beans.factory.annotation.Autowired
import spock.lang.Unroll

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo
import static com.github.tomakehurst.wiremock.client.WireMock.matchingJsonPath
import static com.github.tomakehurst.wiremock.client.WireMock.post
import static com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo
import static io.github.resilience4j.circuitbreaker.CircuitBreaker.State.CLOSED
import static io.github.resilience4j.circuitbreaker.CircuitBreaker.State.OPEN
import static org.springframework.http.HttpHeaders.CONTENT_TYPE
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE

/**
 * @author bslota on 21/04/2022
 */
class ReservationsResilienceIT extends IntegrationSpec implements ReservationsWebSupport {

    @Autowired
    CircuitBreakerRegistry circuitBreakerRegistry

    @Autowired
    WireMockServer wireMockServer

    def setup() {
        closeCircuit()
    }

    def cleanup() {
        wireMockServer.resetRequests()
        closeCircuit()
    }

    @Unroll
    def "locking asset should be retried 5 times in case of #responseCode responses"() {
        given:
            String vehicleId = "asset-to-be-retried"

        and:
            availabilityServiceLockingWillRespondWith(vehicleId, responseCode)

        when:
            makeReservationFor(vehicleId).send()

        then:
            lockRequestWillBeSentToAvailabilityServiceTimes(5, vehicleId)

        where:
            responseCode << [500, 503]
    }


    @Unroll
    def "locking asset should not retry in case of #responseCode responses"() {
        given:
            String vehicleId = "asset-to-be-retried"

        and:
            availabilityServiceLockingWillRespondWith(vehicleId, responseCode)

        when:
            makeReservationFor(vehicleId).send()

        then:
            lockRequestWillBeSentToAvailabilityServiceTimes(1, vehicleId)

        where:
            responseCode << [400, 404, 422]
    }

    @Unroll
    def "locking asset should end up opening circuit after 7 retries given #responseCode response"() {
        given:
            int maxNumberOfRetriesOpeningTheCircuit = 7
            String vehicleId = "circuit-opening-asset"
            availabilityServiceLockingWillRespondWith(vehicleId, responseCode)

        when: "2 asset locks are sent"
            makeReservationFor(vehicleId).send()
            makeReservationFor(vehicleId).send()

        then: "first command will be retired 5 times and second will be retired 2 times"
            lockRequestWillBeSentToAvailabilityServiceTimes(maxNumberOfRetriesOpeningTheCircuit, vehicleId)

        and:
            circuitIsOpen()

        where:
            responseCode << [500, 503]
    }


    void availabilityServiceLockingWillRespondWith(String vehicleId,
                                                   int responseCode) {
        wireMockServer.stubFor(post(urlEqualTo("/api/assets/commands"))
                .withHeader(CONTENT_TYPE, equalTo(APPLICATION_JSON_VALUE))
                .withRequestBody(matchingJsonPath('$.assetId', equalTo(vehicleId)))
                .willReturn(aResponse().withStatus(responseCode)))
    }

    void lockRequestWillBeSentToAvailabilityServiceTimes(int times, String vehicleId) {
        wireMockServer.verify(times, postRequestedFor(urlEqualTo("/api/assets/commands"))
                .withHeader(CONTENT_TYPE, equalTo(APPLICATION_JSON_VALUE))
                .withRequestBody(matchingJsonPath('$.assetId', equalTo(vehicleId))))
    }

    @Unroll
    def "locking asset should not end up opening circuit after #responseCode errors"() {
        given:
            int totalNumberOfRetriesOpeningTheCircuit = 7
            String vehicleId = "non-circuit-opening-asset"
            availabilityServiceLockingWillRespondWith(vehicleId, responseCode)

        when: "7 asset locks are sent"
            (1..totalNumberOfRetriesOpeningTheCircuit).each { makeReservationFor(vehicleId).send() }

        then:
            lockRequestWillBeSentToAvailabilityServiceTimes(totalNumberOfRetriesOpeningTheCircuit, vehicleId)

        and:
            circuitIsClosed()

        where:
            responseCode << [400, 404, 422]
    }

    void closeCircuit() {
        circuitBreakerRegistry.circuitBreaker("availability-client-send").reset()
    }

    boolean circuitIsOpen() {
        circuitIsInState(OPEN)
    }

    boolean circuitIsClosed() {
        circuitIsInState(CLOSED)
    }

    boolean circuitIsInState(CircuitBreaker.State state) {
        circuitBreakerRegistry.circuitBreaker("availability-client-send").getState() == state
    }
}
