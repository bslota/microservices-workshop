package com.bartslota.reservations


import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic

/**
 * @author bslota on 04/01/2022
 */
class RandomUtils {

    static String random(String placeholder) {
        "$placeholder-${randomAlphabetic(10)}"
    }
}
