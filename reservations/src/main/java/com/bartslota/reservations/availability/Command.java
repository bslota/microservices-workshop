package com.bartslota.reservations.availability;

/**
 * @author bslota on 05/01/2022
 */
interface Command {

    String getType();

}
