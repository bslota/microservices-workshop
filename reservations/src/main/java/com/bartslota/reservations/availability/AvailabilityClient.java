package com.bartslota.reservations.availability;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@FeignClient(name = "availability", path = "${availability.api.root-path}")
interface AvailabilityClient {

    @Retry(name = "availability-client-send")
    @CircuitBreaker(name = "availability-client-send")
    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    ResponseEntity<?> send(@RequestBody Command command, @RequestHeader(AUTHORIZATION) String authorization);
}
