package com.bartslota.reservations.availability;

import java.util.function.Predicate;

import feign.FeignException;

public class IsFailure implements Predicate<Throwable> {

    public static final int RETRYABLE_HTTP_ERROR_CODE_LOWER_BOUND = 500;

    @Override
    public boolean test(Throwable ex) {
        if (ex instanceof FeignException) {
            return ((FeignException) ex).status() >= RETRYABLE_HTTP_ERROR_CODE_LOWER_BOUND;
        }
        return true;
    }
}
