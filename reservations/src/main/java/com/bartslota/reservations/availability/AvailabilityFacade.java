package com.bartslota.reservations.availability;

import java.net.SocketTimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import feign.FeignException;

import static java.lang.String.format;
import static org.springframework.http.HttpStatus.REQUEST_TIMEOUT;
import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;

/**
 * @author bslota on 06/01/2022
 */
@Component
public class AvailabilityFacade {

    private static final Logger LOG = LoggerFactory.getLogger(AvailabilityFacade.class);

    private final AvailabilityClient availabilityClient;

    AvailabilityFacade(AvailabilityClient availabilityClient) {
        this.availabilityClient = availabilityClient;
    }

    public ResponseEntity<?> send(Command command, String authorization) {
        try {
            return availabilityClient.send(command, authorization);
        } catch (FeignException ex) {
            LOG.info(format("Exception when executing command %s", command.getType()), ex);
            if (ex.getCause() instanceof SocketTimeoutException) {
                return ResponseEntity.status(REQUEST_TIMEOUT).build();
            } else if (ex.status() > 0 && ex.status() < 500) {
                return ResponseEntity.status(ex.status()).build();
            } else {
                return ResponseEntity.status(SERVICE_UNAVAILABLE).build();
            }
        } catch (Exception ex) {
            return ResponseEntity.status(SERVICE_UNAVAILABLE).build();
        }
    }
}
