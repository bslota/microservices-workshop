package com.bartslota.c4;

record TeamStyle(String backgroundColorHex, String fontColorHex) {

    static TeamStyle teamStyle(String backgroundColorHex, String fontColorHex) {
        return new TeamStyle(backgroundColorHex, fontColorHex);
    }
}
