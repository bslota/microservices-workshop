package com.bartslota.ebike.shared.events;

import java.time.Instant;
import java.util.UUID;

/**
 * @author bslota on 08/01/2021
 */
public interface DomainEvent {

    UUID id();

    Instant occurredAt();

    String type();
}
