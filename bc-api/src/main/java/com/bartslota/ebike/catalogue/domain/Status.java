package com.bartslota.ebike.catalogue.domain;

/**
 * @author bslota on 14/02/2021
 */
public enum Status {
    REGISTERED,
    REMOVED
}
