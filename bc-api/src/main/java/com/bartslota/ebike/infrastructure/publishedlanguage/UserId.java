package com.bartslota.ebike.infrastructure.publishedlanguage;

import java.util.Objects;

/**
 * @author bslota on 05/01/2021
 */
public class UserId {

    private final String id;

    private UserId(String id) {
        this.id = id;
    }

    public static UserId of(String id) {
        return new UserId(id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserId)) return false;
        UserId vehicleId = (UserId) o;
        return Objects.equals(id, vehicleId.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
