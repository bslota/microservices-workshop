package com.bartslota.ebike.infrastructure.publishedlanguage;

import java.util.Objects;

/**
 * @author bslota on 05/01/2021
 */
public class VehicleId {

    private final String id;

    private VehicleId(String id) {
        this.id = id;
    }

    public static VehicleId of(String id) {
        return new VehicleId(id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof VehicleId)) return false;
        VehicleId vehicleId = (VehicleId) o;
        return Objects.equals(id, vehicleId.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public String asString() {
        return id;
    }
}
