package com.bartslota.ebike.lending.domain;

/**
 * @author bslota on 08/01/2021
 */
public interface ReservationRepository {

    void save(Reservation reservation);
}
