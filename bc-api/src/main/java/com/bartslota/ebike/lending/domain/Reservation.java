package com.bartslota.ebike.lending.domain;

import java.time.Duration;
import java.time.LocalDateTime;

import com.bartslota.ebike.infrastructure.publishedlanguage.UserId;

/**
 * @author bslota on 08/01/2021
 */
public class Reservation {

    private final ReservationId reservationId;
    private final BikeId bikeId;
    private final UserId userId;
    private final LocalDateTime validUntil;

    public Reservation(BikeId bikeId, UserId userId, LocalDateTime validUntil) {
        this.reservationId = ReservationId.random();
        this.bikeId = bikeId;
        this.userId = userId;
        this.validUntil = validUntil;
    }

    public static Reservation from(BikeId bikeId, UserId userId, Duration time) {
        return new Reservation(bikeId, userId, LocalDateTime.now().plus(time));
    }
}
