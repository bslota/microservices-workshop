package com.bartslota.ebike.lending.events;

import com.bartslota.ebike.shared.events.BaseDomainEvent;

/**
 * @author bslota on 08/01/2021
 */
public class ReservationMade extends BaseDomainEvent {

    private static final String TYPE = "ReservationMade";


    @Override
    public String type() {
        return TYPE;
    }
}
