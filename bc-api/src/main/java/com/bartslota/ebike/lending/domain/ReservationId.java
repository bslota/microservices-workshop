package com.bartslota.ebike.lending.domain;

import java.util.Objects;
import java.util.UUID;

/**
 * @author bslota on 05/01/2021
 */
public class ReservationId {

    private final UUID id;

    private ReservationId(UUID id) {
        this.id = id;
    }

    public static ReservationId random() {
        return new ReservationId(UUID.randomUUID());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ReservationId)) return false;
        ReservationId vehicleId = (ReservationId) o;
        return Objects.equals(id, vehicleId.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
