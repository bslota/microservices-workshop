package com.bartslota.ebike.lending.application;

import java.time.Duration;

import com.bartslota.ebike.availability.application.AvailabilityService;
import com.bartslota.ebike.availability.domain.AssetId;
import com.bartslota.ebike.availability.domain.OwnerId;
import com.bartslota.ebike.infrastructure.publishedlanguage.UserId;
import com.bartslota.ebike.lending.domain.BikeId;
import com.bartslota.ebike.lending.domain.Reservation;
import com.bartslota.ebike.lending.domain.ReservationRepository;
import com.bartslota.ebike.lending.events.ReservationMade;
import com.bartslota.ebike.shared.events.DomainEventsPublisher;

import static java.time.temporal.ChronoUnit.MINUTES;

class ReservationHandlerOption1 {

    private static final Duration RESERVATION_HOLDING_TIME = Duration.of(10, MINUTES);
    private final AvailabilityService availabilityService;
    private final ReservationRepository reservationRepository;
    private final DomainEventsPublisher domainEventsPublisher;

    ReservationHandlerOption1(AvailabilityService availabilityService, ReservationRepository reservationRepository, DomainEventsPublisher domainEventsPublisher) {
        this.availabilityService = availabilityService;
        this.reservationRepository = reservationRepository;
        this.domainEventsPublisher = domainEventsPublisher;
    }

    //orchiestration
    public void handle(MakeReservation command) {
        //change availability - is it needed here?
        //what about calling availability service right away and listening to availability events?
        availabilityService.lock(AssetId.of(command.bikeId()),
                OwnerId.of(command.userId()),
                RESERVATION_HOLDING_TIME);

        //create and store reservation
        //do I need to have it? Isn't availability enough?
        //Does availability know what is the reason for locking? Should it know anything about it?
        //If it was a person from operations that locked/unlocked the asset, how do I know if there was any reservation? We need to keep the model here
        Reservation reservation = Reservation.from(
                BikeId.of(command.bikeId()),
                UserId.of(command.userId()),
                RESERVATION_HOLDING_TIME);
        reservationRepository.save(reservation);

        //emit reservation event
        domainEventsPublisher.publish(new ReservationMade());
    }




    /*
    //lockVehicle - now what if I manage to lock the resource but the lock on the station fails?
        //if this was a result of handling availability event, we could just re-read the event
        //and try to lock the vehicle again
        //if locking is not possible, we should reject reservation
        LockVehicle lockVehicleCommand = LockVehicle.of(VehicleId.of(command.bikeId()), UserId.of(command.userId()));
        infrastructureHandler.handle(lockVehicleCommand);

        domainEventsPublisher.publish(ReservationMade.from(reservation));
        //pricingService.takeInitialFeeFor(command.userId()) - what if i manage to do what's above but payment service is down?
        //domainEventsPublisher.publish(reservationMade); - we could take payment here - ReservationPaymentService could do the translation
     */
}
