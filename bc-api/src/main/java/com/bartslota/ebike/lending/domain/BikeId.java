package com.bartslota.ebike.lending.domain;

import java.util.Objects;

/**
 * @author bslota on 05/01/2021
 */
public class BikeId {

    private final String id;

    private BikeId(String id) {
        this.id = id;
    }

    public static BikeId of(String id) {
        return new BikeId(id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BikeId)) return false;
        BikeId vehicleId = (BikeId) o;
        return Objects.equals(id, vehicleId.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
