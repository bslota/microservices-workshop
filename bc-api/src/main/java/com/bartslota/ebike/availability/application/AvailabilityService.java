package com.bartslota.ebike.availability.application;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.function.Consumer;

import org.springframework.transaction.annotation.Transactional;

import com.bartslota.ebike.availability.domain.AssetAvailability;
import com.bartslota.ebike.availability.domain.AssetAvailabilityRepository;
import com.bartslota.ebike.availability.domain.AssetId;
import com.bartslota.ebike.availability.domain.OwnerId;
import com.bartslota.ebike.availability.events.AssetLocked;
import com.bartslota.ebike.availability.events.AssetRegistered;
import com.bartslota.ebike.availability.events.AssetUnlocked;
import com.bartslota.ebike.availability.events.AssetWithdrawn;
import com.bartslota.ebike.shared.events.DomainEventsPublisher;

/**
 * @author bslota on 07/01/2021
 */
@Transactional
public class AvailabilityService {

    private final AssetAvailabilityRepository repository;
    private final DomainEventsPublisher domainEventsPublisher;

    AvailabilityService(AssetAvailabilityRepository repository, DomainEventsPublisher domainEventsPublisher) {
        this.repository = repository;
        this.domainEventsPublisher = domainEventsPublisher;
    }

    public void register(AssetId assetId) {
        repository.save(AssetAvailability.of(assetId)); //SHOULD it be available right away? Or should we wait for the station to give a sign that it is unlocked?
        domainEventsPublisher.publish(AssetRegistered.from(assetId));
    }

    public void withdraw(AssetId assetId) {
        repository
                .findBy(assetId)
                .ifPresent(this::handleWithdrawalOf);
    }

    public void lock(AssetId assetId, OwnerId ownerId, Duration time) {
        repository
                .findBy(assetId)
                .ifPresentOrElse(
                        handleLockFor(ownerId, time),
                        raiseAssetNotAvailable()
                );
    }

    public void lockIndefinitely(AssetId assetId, OwnerId ownerId) {
        repository
                .findBy(assetId)
                .ifPresentOrElse(
                        handleIndefiniteLockFor(ownerId),
                        raiseAssetNotAvailable()
                );
    }

    public void unlock(AssetId assetId, OwnerId ownerId, LocalDateTime at) {
        repository
                .findBy(assetId)
                .ifPresent(
                        handleUnlockFor(ownerId, at)
                );
    }

    public void unlockIfOverdue(AssetAvailability assetAvailability) {
        assetAvailability.unlockIfOverdue()
                         .ifPresent(event -> {
                             repository.save(assetAvailability);
                             domainEventsPublisher.publish(event);
                         });
    }

    private void handleWithdrawalOf(AssetAvailability assetAvailability) {
        AssetWithdrawn event = assetAvailability.withdraw();
        repository.save(assetAvailability);
        domainEventsPublisher.publish(event);
    }

    private Consumer<AssetAvailability> handleLockFor(OwnerId ownerId, Duration time) {
        return assetAvailability -> {
            AssetLocked event = assetAvailability.lockFor(ownerId, time);
            repository.save(assetAvailability);
            domainEventsPublisher.publish(event);
        };
    }

    private Consumer<AssetAvailability> handleIndefiniteLockFor(OwnerId ownerId) {
        return assetAvailability -> {
            AssetLocked event = assetAvailability.lockIndefinitelyFor(ownerId);
            repository.save(assetAvailability);
            domainEventsPublisher.publish(event);
        };
    }

    private Consumer<AssetAvailability> handleUnlockFor(OwnerId ownerId, LocalDateTime at) {
        return assetAvailability -> {
            AssetUnlocked event = assetAvailability.unlockFor(ownerId, at);
            repository.save(assetAvailability);
            domainEventsPublisher.publish(event);
        };
    }

    private Runnable raiseAssetNotAvailable() {
        return () -> {throw new IllegalArgumentException("Asset is not registered");};
    }

}
