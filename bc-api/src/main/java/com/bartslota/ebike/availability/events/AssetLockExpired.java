package com.bartslota.ebike.availability.events;

import com.bartslota.ebike.availability.domain.AssetId;
import com.bartslota.ebike.shared.events.BaseDomainEvent;

/**
 * @author bslota on 08/01/2021
 */
public class AssetLockExpired extends BaseDomainEvent {

    private static final String TYPE = "AssetLockExpired";

    private final AssetId assetId;

    private AssetLockExpired(AssetId assetId) {
        super();
        this.assetId = assetId;
    }

    public static AssetLockExpired from(AssetId assetId) {
        return new AssetLockExpired(assetId);
    }

    @Override
    public String type() {
        return TYPE;
    }
}
