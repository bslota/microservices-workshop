package com.bartslota.ebike.availability.events;

import com.bartslota.ebike.availability.domain.AssetId;
import com.bartslota.ebike.shared.events.BaseDomainEvent;

/**
 * @author bslota on 08/01/2021
 */
public class AssetWithdrawn extends BaseDomainEvent {

    private static final String TYPE = "AssetWithdrawn";

    private final AssetId assetId;

    private AssetWithdrawn(AssetId assetId) {
        super();
        this.assetId = assetId;
    }

    public static AssetWithdrawn from(AssetId assetId) {
        return new AssetWithdrawn(assetId);
    }

    @Override
    public String type() {
        return TYPE;
    }
}
