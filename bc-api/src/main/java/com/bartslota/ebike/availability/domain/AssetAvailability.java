package com.bartslota.ebike.availability.domain;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Optional;

import com.bartslota.ebike.availability.events.AssetLockExpired;
import com.bartslota.ebike.availability.events.AssetLocked;
import com.bartslota.ebike.availability.events.AssetUnlocked;
import com.bartslota.ebike.availability.events.AssetWithdrawn;

/**
 * @author bslota on 08/01/2021
 */
public class AssetAvailability {

    private AssetId assetId;
    private Lock currentLock = new MaintenanceLock();
    private static final Duration INDEFINITE_LOCK_DURATION = Duration.ofDays(365);

    private AssetAvailability(AssetId assetId) {
        this.assetId = assetId;
    }

    public static AssetAvailability of(AssetId assetId) {
        return new AssetAvailability(assetId);
    }

    public AssetWithdrawn withdraw() {
        if (currentLock == null) {
            currentLock = new WithdrawalLock();
            return AssetWithdrawn.from(assetId);
        }
        throw new IllegalStateException("Asset is currently locked");
    }

    public AssetLocked lockFor(OwnerId ownerId, Duration time) {
        if (currentLock == null) { //we could handle lock reclaim for idempotency
            LocalDateTime validUntil = LocalDateTime.now().plus(time);
            currentLock = new OwnerLock(ownerId, validUntil);
            return AssetLocked.from(this.assetId, ownerId, validUntil);
        }
        throw new IllegalStateException("Asset is currently locked");
    }

    public AssetLocked lockIndefinitelyFor(OwnerId ownerId) {
        return lockFor(ownerId, INDEFINITE_LOCK_DURATION);
    }

    public AssetUnlocked unlockFor(OwnerId ownerId, LocalDateTime at) {
        if (thereIsAnActiveLockFor(ownerId)) {
            currentLock = null;
            return AssetUnlocked.from(this.assetId, ownerId, at);
        }
        throw new IllegalStateException("Asset is currently locked");
    }

    public Optional<AssetLockExpired> unlockIfOverdue() {
        if (currentLock != null) {
            currentLock = null;
            return Optional.of(AssetLockExpired.from(assetId));
        }
        return Optional.empty();
    }

    private boolean thereIsAnActiveLockFor(OwnerId ownerId) {
        return Optional.ofNullable(currentLock).filter(lock -> lock.wasMadeFor(ownerId)).isPresent();
    }

    private interface Lock {

        OwnerId ownerId();

        default boolean wasMadeFor(OwnerId ownerId) {
            return ownerId().equals(ownerId);
        }
    }

    private static final class WithdrawalLock implements Lock {

        private static final OwnerId WITHDRAWAL_OWNER_ID = OwnerId.of("WITHDRAWAL");

        @Override
        public OwnerId ownerId() {
            return WITHDRAWAL_OWNER_ID;
        }

    }

    private static final class MaintenanceLock implements Lock {

        private static final OwnerId MAINTENANCE_OWNER_ID = OwnerId.of("MAINTENANCE");

        @Override
        public OwnerId ownerId() {
            return MAINTENANCE_OWNER_ID;
        }

    }

    private static final class OwnerLock implements Lock {

        private final OwnerId ownerId;
        private final LocalDateTime until;

        OwnerLock(OwnerId ownerId, LocalDateTime until) {
            this.ownerId = ownerId;
            this.until = until;
        }

        @Override
        public OwnerId ownerId() {
            return ownerId;
        }
    }

}
