package com.bartslota.availability.events;

import java.time.Instant;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author bslota on 08/01/2021
 */
public class AssetLockedIndefinitely extends BaseDomainEvent {

    @JsonIgnore
    static final String TYPE = "AssetLockedIndefinitely";

    private final String assetId;
    private final String ownerId;

    @JsonCreator
    private AssetLockedIndefinitely(String assetId, String ownerId, Instant occurredAt) {
        super(occurredAt);
        this.assetId = assetId;
        this.ownerId = ownerId;
    }

    public static AssetLockedIndefinitely from(String assetId, String ownerId, Instant occurrenceTime) {
        return new AssetLockedIndefinitely(assetId, ownerId, occurrenceTime);
    }

    @Override
    public String getType() {
        return TYPE;
    }

    String getAssetId() {
        return assetId;
    }

    String getOwnerId() {
        return ownerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AssetLockedIndefinitely)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        AssetLockedIndefinitely that = (AssetLockedIndefinitely) o;
        return Objects.equals(assetId, that.assetId) && Objects.equals(ownerId, that.ownerId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), assetId, ownerId);
    }
}
