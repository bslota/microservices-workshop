package com.bartslota.availability.events;

import java.time.Instant;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author bslota on 08/01/2021
 */
public class AssetLockRejected extends BaseDomainEvent {

    @JsonIgnore
    static final String TYPE = "AssetLockRejected";

    private final String assetId;
    private final String ownerId;
    private final String reason;

    @JsonCreator
    private AssetLockRejected(String assetId, String ownerId, String reason, Instant occurredAt) {
        super(occurredAt);
        this.assetId = assetId;
        this.ownerId = ownerId;
        this.reason = reason;
    }

    public static AssetLockRejected dueToAssetUnavailabilityWith(String assetId, String ownerId, Instant occurrenceTime) {
        return new AssetLockRejected(assetId, ownerId, "ASSET_NOT_AVAILABLE", occurrenceTime);
    }

    public static AssetLockRejected dueToMissingAssetWith(String assetId, String ownerId, Instant occurrenceTime) {
        return new AssetLockRejected(assetId, ownerId, "ASSET_IS_MISSING", occurrenceTime);
    }

    public static AssetLockRejected dueToNoLockBeingDefinedForOwnerWith(String assetId, String ownerId, Instant occurrenceTime) {
        return new AssetLockRejected(assetId, ownerId, "NO_LOCK_DEFINED_FOR_OWNER", occurrenceTime);
    }

    @Override
    public String getType() {
        return TYPE;
    }

    String getAssetId() {
        return assetId;
    }

    String getOwnerId() {
        return ownerId;
    }

    String getReason() {
        return reason;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AssetLockRejected)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        AssetLockRejected that = (AssetLockRejected) o;
        return Objects.equals(assetId, that.assetId) && Objects.equals(ownerId, that.ownerId) && Objects.equals(reason, that.reason);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), assetId, ownerId, reason);
    }
}
