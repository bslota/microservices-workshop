package com.bartslota.availability.events;

import java.time.Instant;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author bslota on 08/01/2021
 */
public class AssetUnlockingIgnored extends BaseDomainEvent {

    @JsonIgnore
    static final String TYPE = "AssetUnlockingIgnored";

    private final String assetId;

    @JsonCreator
    private AssetUnlockingIgnored(String assetId, Instant occurredAt) {
        super(occurredAt);
        this.assetId = assetId;
    }

    public static AssetUnlockingIgnored from(String assetId, Instant occurrenceTime) {
        return new AssetUnlockingIgnored(assetId, occurrenceTime);
    }

    @Override
    public String getType() {
        return TYPE;
    }

    String getAssetId() {
        return assetId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AssetUnlockingIgnored)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        AssetUnlockingIgnored that = (AssetUnlockingIgnored) o;
        return Objects.equals(assetId, that.assetId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), assetId);
    }
}
