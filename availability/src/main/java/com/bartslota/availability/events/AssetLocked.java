package com.bartslota.availability.events;

import java.time.Instant;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author bslota on 08/01/2021
 */
public class AssetLocked extends BaseDomainEvent {

    @JsonIgnore
    static final String TYPE = "AssetLocked";

    private final String assetId;
    private final String ownerId;
    private final Instant validUntil;

    @JsonCreator
    private AssetLocked(String assetId, String ownerId, Instant validUntil, Instant occurredAt) {
        super(occurredAt);
        this.assetId = assetId;
        this.ownerId = ownerId;
        this.validUntil = validUntil;
    }

    public static AssetLocked from(String assetId, String ownerId, Instant validUntil, Instant occurrenceTime) {
        return new AssetLocked(assetId, ownerId, validUntil, occurrenceTime);
    }

    @Override
    public String getType() {
        return TYPE;
    }

    String getAssetId() {
        return assetId;
    }

    String getOwnerId() {
        return ownerId;
    }

    Instant getValidUntil() {
        return validUntil;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AssetLocked)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        AssetLocked that = (AssetLocked) o;
        return Objects.equals(assetId, that.assetId) && Objects.equals(ownerId, that.ownerId) && Objects.equals(validUntil, that.validUntil);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), assetId, ownerId, validUntil);
    }
}
