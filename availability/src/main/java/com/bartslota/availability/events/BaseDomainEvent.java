package com.bartslota.availability.events;

import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

/**
 * @author bslota on 08/01/2021
 */
public abstract class BaseDomainEvent implements DomainEvent {

    private final UUID id;
    private final Instant occurredAt;

    public BaseDomainEvent(Instant occurredAt) {
        this.id = UUID.randomUUID();
        this.occurredAt = occurredAt;
    }

    @Override
    public UUID getId() {
        return id;
    }

    @Override
    public Instant getOccurredAt() {
        return occurredAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BaseDomainEvent)) {
            return false;
        }
        BaseDomainEvent that = (BaseDomainEvent) o;
        return Objects.equals(occurredAt, that.occurredAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(occurredAt);
    }
}
