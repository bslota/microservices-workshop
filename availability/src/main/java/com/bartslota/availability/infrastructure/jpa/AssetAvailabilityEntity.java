package com.bartslota.availability.infrastructure.jpa;

import java.time.LocalDateTime;
import java.time.ZoneId;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.bartslota.availability.domain.AssetAvailability;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import static com.fasterxml.jackson.annotation.JsonTypeInfo.Id.NAME;

/**
 * @author bslota on 30/11/2021
 */
@Entity
@Table(name = "asset_availability")
class AssetAvailabilityEntity {

    @Column
    @Id
    String assetId;

    @Column
    String lock;

    AssetAvailabilityEntity() {
    }

    AssetAvailabilityEntity(String assetId, String lock) {
        this.assetId = assetId;
        this.lock = lock;
    }

    String getAssetId() {
        return assetId;
    }

    String getLock() {
        return lock;
    }

    void setAssetId(String assetId) {
        this.assetId = assetId;
    }

    void setLock(String lock) {
        this.lock = lock;
    }

    @JsonTypeInfo(property = "type", use = NAME)
    @JsonSubTypes({
            @JsonSubTypes.Type(value = WithdrawalLockRepresentation.class, name = WithdrawalLockRepresentation.TYPE),
            @JsonSubTypes.Type(value = MaintenanceLockRepresentation.class, name = MaintenanceLockRepresentation.TYPE),
            @JsonSubTypes.Type(value = OwnerLockRepresentation.class, name = OwnerLockRepresentation.TYPE),
            @JsonSubTypes.Type(value = IndefiniteOwnerLockRepresentation.class, name = IndefiniteOwnerLockRepresentation.TYPE)
    })

    sealed interface LockRepresentation
            permits WithdrawalLockRepresentation, MaintenanceLockRepresentation,
                    OwnerLockRepresentation, IndefiniteOwnerLockRepresentation {

        static LockRepresentation from(AssetAvailability.Lock lock) {
            return switch (lock) {
                case AssetAvailability.WithdrawalLock l -> WithdrawalLockRepresentation.from(l);
                case AssetAvailability.MaintenanceLock l -> MaintenanceLockRepresentation.from(l);
                case AssetAvailability.OwnerLock l -> OwnerLockRepresentation.from(l);
                case AssetAvailability.IndefiniteOwnerLock l -> IndefiniteOwnerLockRepresentation.from(l);
            };
        }
    }

    final static record WithdrawalLockRepresentation(String ownerId) implements LockRepresentation {

        static final String TYPE = "WITHDRAWAL_LOCK";

        static WithdrawalLockRepresentation from(AssetAvailability.WithdrawalLock lock) {
            return new WithdrawalLockRepresentation(lock.ownerId().asString());
        }
    }

    final static record MaintenanceLockRepresentation(String ownerId) implements LockRepresentation {

        static final String TYPE = "MAINTENANCE_LOCK";

        static MaintenanceLockRepresentation from(AssetAvailability.MaintenanceLock lock) {
            return new MaintenanceLockRepresentation(lock.ownerId().asString());
        }
    }

    final static record OwnerLockRepresentation(String ownerId, LocalDateTime until) implements LockRepresentation {

        static final String TYPE = "OWNER_LOCK";

        static OwnerLockRepresentation from(AssetAvailability.OwnerLock lock) {
            return new OwnerLockRepresentation(lock.ownerId().asString(), LocalDateTime.ofInstant(lock.validUntil(), ZoneId.systemDefault()));
        }
    }

    final static record IndefiniteOwnerLockRepresentation(String ownerId) implements LockRepresentation {

        static final String TYPE = "INDEFINITE_OWNER_LOCK";

        static IndefiniteOwnerLockRepresentation from(AssetAvailability.IndefiniteOwnerLock lock) {
            return new IndefiniteOwnerLockRepresentation(lock.ownerId().asString());
        }
    }
}
