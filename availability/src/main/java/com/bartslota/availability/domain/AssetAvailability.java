package com.bartslota.availability.domain;

import java.time.Duration;
import java.time.Instant;
import java.util.Objects;
import java.util.Optional;

import com.bartslota.availability.events.AssetActivated;
import com.bartslota.availability.events.AssetActivationRejected;
import com.bartslota.availability.events.AssetLockExpired;
import com.bartslota.availability.events.AssetLockRejected;
import com.bartslota.availability.events.AssetLocked;
import com.bartslota.availability.events.AssetLockedIndefinitely;
import com.bartslota.availability.events.AssetUnlocked;
import com.bartslota.availability.events.AssetUnlockingIgnored;
import com.bartslota.availability.events.AssetUnlockingRejected;
import com.bartslota.availability.events.AssetWithdrawalRejected;
import com.bartslota.availability.events.AssetWithdrawn;

import io.vavr.control.Either;

import static io.vavr.control.Either.left;
import static io.vavr.control.Either.right;

/**
 * @author bslota on 08/01/2021
 */
public class AssetAvailability {

    private final TimeProvider timeProvider;
    private final AssetId assetId;
    private Lock currentLock = new MaintenanceLock();

    private AssetAvailability(TimeProvider timeProvider, AssetId assetId) {
        this.timeProvider = timeProvider;
        this.assetId = assetId;
    }

    public static AssetAvailability of(AssetId assetId, TimeProvider timeProvider) {
        return new AssetAvailability(timeProvider, assetId);
    }

    public Either<AssetActivationRejected, AssetActivated> activate() {
        if (currentLock instanceof MaintenanceLock) {
            this.currentLock = null;
            return right(AssetActivated.from(assetId.asString(), timeProvider.instant()));
        }
        return left(AssetActivationRejected.dueToMissingAssetWith(assetId.asString(), timeProvider.instant()));
    }

    public Either<AssetLockRejected, AssetLocked> lockFor(OwnerId ownerId, Duration duration) {
        if (currentLock == null) {
            Instant validUntil = timeProvider.instant().plus(duration);
            currentLock = new OwnerLock(ownerId, validUntil);
            return right(AssetLocked.from(assetId.asString(), ownerId.asString(), validUntil, timeProvider.instant()));
        }
        return left(AssetLockRejected.dueToAssetUnavailabilityWith(assetId.asString(), ownerId.asString(), timeProvider.instant()));
    }

    public Either<AssetLockRejected, AssetLockedIndefinitely> lockIndefinitelyFor(OwnerId ownerId) {
        if (thereIsAnActiveLockFor(ownerId)) {
            currentLock = new IndefiniteOwnerLock(ownerId);
            return right(AssetLockedIndefinitely.from(assetId.asString(), ownerId.asString(), timeProvider.instant()));
        }
        return left(AssetLockRejected.dueToNoLockBeingDefinedForOwnerWith(assetId.asString(), ownerId.asString(), timeProvider.instant()));
    }

    public Either<AssetUnlockingRejected, AssetUnlocked> unlockFor(OwnerId ownerId) {
        if (thereIsAnActiveLockFor(ownerId)) {
            currentLock = null;
            return right(AssetUnlocked.from(assetId.asString(), ownerId.asString(), timeProvider.instant()));
        }
        return left(AssetUnlockingRejected.dueToMissingLockOnTheAssetWith(assetId.asString(), ownerId.asString(), timeProvider.instant()));
    }

    public Either<AssetUnlockingIgnored, AssetLockExpired> unlockIfOverdue() {
        if (thereIsAnOverdueLock()) {
            currentLock = null;
            return right(AssetLockExpired.from(assetId.asString(), timeProvider.instant()));
        }
        return left(AssetUnlockingIgnored.from(assetId.asString(), timeProvider.instant()));
    }

    public Either<AssetWithdrawalRejected, AssetWithdrawn> withdraw() {
        if (currentLock == null || currentLock instanceof MaintenanceLock) {
            this.currentLock = new WithdrawalLock();
            return right(AssetWithdrawn.from(assetId.asString(), timeProvider.instant()));
        }
        return left(AssetWithdrawalRejected.dueToAssetBeingLockedWith(assetId.asString(), timeProvider.instant()));
    }

    private boolean thereIsAnActiveLockFor(OwnerId ownerId) {
        return Optional.ofNullable(currentLock)
                       .filter(lock -> lock.isValidConsidering(timeProvider))
                       .filter(lock -> lock.wasMadeFor(ownerId))
                       .isPresent();
    }

    private boolean thereIsAnOverdueLock() {
        return Optional.ofNullable(currentLock)
                       .filter(lock -> !lock.isValidConsidering(timeProvider))
                       .isPresent();
    }

    public AssetId id() {
        return this.assetId;
    }

    public Optional<Lock> currentLock() {
        return Optional.ofNullable(currentLock);
    }

    public AssetAvailability with(Lock lock) {
        this.currentLock = lock;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AssetAvailability)) {
            return false;
        }
        AssetAvailability that = (AssetAvailability) o;
        return Objects.equals(assetId, that.assetId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(assetId);
    }

    public sealed interface Lock permits WithdrawalLock, MaintenanceLock, OwnerLock, IndefiniteOwnerLock {

        OwnerId ownerId();

        boolean isValidConsidering(TimeProvider timeProvider);

        default boolean wasMadeFor(OwnerId ownerId) {
            return ownerId().equals(ownerId);
        }
    }

    public record OwnerLock(OwnerId ownerId, Instant validUntil) implements Lock {

        @Override
        public boolean isValidConsidering(TimeProvider timeProvider) {
            return !timeProvider.get().instant().isAfter(validUntil);
        }
    }

    public record IndefiniteOwnerLock(OwnerId ownerId) implements Lock {

        @Override
        public boolean isValidConsidering(TimeProvider timeProvider) {
            return true;
        }
    }

    public static final class WithdrawalLock implements Lock {

        private static final OwnerId WITHDRAWAL_OWNER_ID = OwnerId.of("WITHDRAWAL");

        @Override
        public OwnerId ownerId() {
            return WITHDRAWAL_OWNER_ID;
        }

        @Override
        public boolean isValidConsidering(TimeProvider timeProvider) {
            return true;
        }

    }

    public static final class MaintenanceLock implements Lock {

        private static final OwnerId MAINTENANCE_OWNER_ID = OwnerId.of("MAINTENANCE");

        @Override
        public OwnerId ownerId() {
            return MAINTENANCE_OWNER_ID;
        }

        @Override
        public boolean isValidConsidering(TimeProvider timeProvider) {
            return true;
        }

    }
}
