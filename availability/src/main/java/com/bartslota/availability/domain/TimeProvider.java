package com.bartslota.availability.domain;

import java.time.Instant;
import java.time.InstantSource;
import java.util.function.Supplier;

/**
 * @author bslota on 19/03/2022
 */
public interface TimeProvider extends Supplier<InstantSource> {

    default Instant instant() {
        return get().instant();
    }
}
