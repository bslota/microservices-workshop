package com.bartslota.availability.application;

import java.time.Duration;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

import com.bartslota.availability.domain.AssetAvailability;
import com.bartslota.availability.domain.AssetAvailabilityRepository;
import com.bartslota.availability.domain.AssetId;
import com.bartslota.availability.domain.OwnerId;
import com.bartslota.availability.domain.TimeProvider;
import com.bartslota.availability.events.AssetActivated;
import com.bartslota.availability.events.AssetActivationRejected;
import com.bartslota.availability.events.AssetLockExpired;
import com.bartslota.availability.events.AssetLockRejected;
import com.bartslota.availability.events.AssetLocked;
import com.bartslota.availability.events.AssetLockedIndefinitely;
import com.bartslota.availability.events.AssetRegistered;
import com.bartslota.availability.events.AssetRegistrationRejected;
import com.bartslota.availability.events.AssetUnlocked;
import com.bartslota.availability.events.AssetUnlockingIgnored;
import com.bartslota.availability.events.AssetUnlockingRejected;
import com.bartslota.availability.events.AssetWithdrawalRejected;
import com.bartslota.availability.events.AssetWithdrawn;
import com.bartslota.availability.events.DomainEvent;
import com.bartslota.availability.events.DomainEventsPublisher;

import io.vavr.control.Either;

/**
 * @author bslota on 07/01/2021
 */
@Service
@Transactional
public class AvailabilityService {

    private final AssetAvailabilityRepository repository;
    private final DomainEventsPublisher domainEventsPublisher;
    private final TimeProvider timeProvider;

    AvailabilityService(AssetAvailabilityRepository repository, DomainEventsPublisher domainEventsPublisher, TimeProvider timeProvider) {
        this.repository = repository;
        this.domainEventsPublisher = domainEventsPublisher;
        this.timeProvider = timeProvider;
    }

    @Secured("ROLE_ADMIN")
    public Either<AssetRegistrationRejected, AssetRegistered> registerAssetWith(AssetId assetId) {
        Optional<AssetAvailability> asset = repository.findBy(assetId);
        if (asset.isEmpty()) {
            repository.save(AssetAvailability.of(assetId, timeProvider));
            AssetRegistered event = AssetRegistered.from(assetId.asString(), timeProvider.instant());
            domainEventsPublisher.publish(event);
            return Either.right(event);
        }
        return Either.left(AssetRegistrationRejected.dueToAlreadyExistingAssetWith(assetId.asString(), timeProvider.instant()));
    }

    @Secured("ROLE_ADMIN")
    public Either<AssetActivationRejected, AssetActivated> activate(AssetId assetId) {
        return repository.findBy(assetId)
                         .map(asset -> handle(asset, asset.activate()))
                         .orElse(Either.left(AssetActivationRejected.dueToMissingAssetWith(assetId.asString(), timeProvider.instant())));
    }

    @Secured("ROLE_ADMIN")
    public Either<AssetWithdrawalRejected, AssetWithdrawn> withdraw(AssetId assetId) {
        return repository.findBy(assetId)
                         .map(asset -> handle(asset, asset.withdraw()))
                         .orElse(Either.left(AssetWithdrawalRejected.dueToMissingAssetWith(assetId.asString(), timeProvider.instant())));
    }

    @Secured("ROLE_CUSTOMER")
    public Either<AssetLockRejected, AssetLocked> lock(AssetId assetId, OwnerId ownerId, Duration duration) {
        return repository.findBy(assetId)
                         .map(asset -> handle(asset, asset.lockFor(ownerId, duration)))
                         .orElse(Either.left(AssetLockRejected.dueToMissingAssetWith(assetId.asString(), ownerId.asString(), timeProvider.instant())));
    }

    @Secured("ROLE_CUSTOMER")
    public Either<AssetLockRejected, AssetLockedIndefinitely> lockIndefinitely(AssetId assetId, OwnerId ownerId) {
        return repository.findBy(assetId)
                         .map(asset -> handle(asset, asset.lockIndefinitelyFor(ownerId)))
                         .orElse(Either.left(AssetLockRejected.dueToMissingAssetWith(assetId.asString(), ownerId.asString(), timeProvider.instant())));
    }

    @Secured("ROLE_CUSTOMER")
    public Either<AssetUnlockingRejected, AssetUnlocked> unlock(AssetId assetId, OwnerId ownerId) {
        return repository.findBy(assetId)
                         .map(asset -> handle(asset, asset.unlockFor(ownerId)))
                         .orElse(Either.left(AssetUnlockingRejected.dueToMissingAssetWith(assetId.asString(), ownerId.asString(), timeProvider.instant())));
    }

    public void unlockIfOverdue(AssetAvailability assetAvailability) {
        Either<AssetUnlockingIgnored, AssetLockExpired> result = assetAvailability.unlockIfOverdue();
        result.map(success -> {
            repository.save(assetAvailability);
            domainEventsPublisher.publish(success);
            return success;
        });

    }

    private <T extends DomainEvent, U extends DomainEvent> Either<T, U> handle(AssetAvailability asset, Either<T, U> executionResult) {
        if (executionResult.isRight()) {
            repository.save(asset);
            domainEventsPublisher.publish(executionResult.get());
        }
        return executionResult;
    }

}
