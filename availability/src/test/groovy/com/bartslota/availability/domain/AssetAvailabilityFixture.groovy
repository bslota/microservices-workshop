package com.bartslota.availability.domain

import com.bartslota.availability.domain.TimeProviderFixture.TestTimeProvider

import java.time.Duration
import java.time.Instant

import static com.bartslota.availability.domain.AssetIdFixture.someAssetId
import static com.bartslota.availability.domain.DurationFixture.someValidDuration
import static com.bartslota.availability.domain.OwnerIdFixture.someOwnerId
import static com.bartslota.availability.domain.TimeProviderFixture.timeProvider

/**
 * @author bslota on 22/11/2021
 */
class AssetAvailabilityFixture {

    static AssetAvailability someNewAsset() {
        AssetAvailability.of(someAssetId(), timeProvider())
    }

    static AssetAvailabilityBuilder someAsset() {
        new AssetAvailabilityBuilder().with(someAssetId())
    }

    static class AssetAvailabilityBuilder {

        private AssetAvailability assetAvailability
        private OwnerId lastLockOwnerId
        private TestTimeProvider testTimeProvider = new TestTimeProvider()

        AssetAvailabilityBuilder with(AssetId assetId) {
            assetAvailability = AssetAvailability.of(assetId, testTimeProvider)
            this
        }

        AssetAvailabilityBuilder thatIsActive() {
            assetAvailability.activate()
            this
        }

        AssetAvailabilityBuilder withClockSetTo(Instant time) {
            testTimeProvider.thatIsFixedFor(time)
            this
        }

        AssetAvailabilityLockBuilder thatWasLockedByOwnerWith(OwnerId ownerId) {
            new AssetAvailabilityLockBuilder(this, ownerId)
        }

        AssetAvailabilityLockBuilder thatWasLockedBySomeOwner() {
            lastLockOwnerId = someOwnerId()
            new AssetAvailabilityLockBuilder(this, lastLockOwnerId)
        }

        AssetAvailabilityBuilder thenUnlocked() {
            assetAvailability.unlockFor(lastLockOwnerId)
            this
        }

        AssetAvailabilityBuilder thenLockedIndefinitely() {
            assetAvailability.lockIndefinitelyFor(lastLockOwnerId)
            this
        }

        AssetAvailabilityBuilder thatIsWithdrawn() {
            assetAvailability.withdraw()
            this
        }

        AssetAvailability get() {
            assetAvailability
        }

        private AssetAvailabilityBuilder executeOnAsset(Closure assetFunction) {
            assetFunction(assetAvailability)
            this
        }
    }

    static class AssetAvailabilityLockBuilder {
        private AssetAvailabilityBuilder parent
        private OwnerId lockOwnerId
        private Instant lockTime

        AssetAvailabilityLockBuilder(AssetAvailabilityBuilder parent, OwnerId ownerId) {
            this.parent = parent
            this.lockOwnerId = ownerId
        }

        AssetAvailabilityBuilder forSomeValidDuration() {
            parent.executeOnAsset {
                AssetAvailability asset -> asset.lockFor(lockOwnerId, someValidDuration())
            }
        }

        AssetAvailabilityBuilder forDurationOf(Duration duration) {
            parent.executeOnAsset {
                AssetAvailability asset -> asset.lockFor(lockOwnerId, duration)
            }
        }
    }
}
