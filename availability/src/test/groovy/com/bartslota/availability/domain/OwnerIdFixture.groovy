package com.bartslota.availability.domain


import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric

/**
 * @author bslota on 19/03/2022
 */
class OwnerIdFixture {

    static OwnerId someOwnerId() {
        OwnerId.of(someOwnerIdValue())
    }

    static String someOwnerIdValue() {
        randomAlphanumeric(10)
    }
}
