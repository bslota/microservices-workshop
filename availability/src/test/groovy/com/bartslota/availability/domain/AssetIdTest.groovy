package com.bartslota.availability.domain

import spock.lang.Specification

import static com.bartslota.availability.domain.AssetIdFixture.someAssetIdValue

/**
 * @author bslota on 19/03/2022
 */
class AssetIdTest extends Specification {

    def "two asset ids should be equal when created from the same value"() {
        given:
            String someValue = someAssetIdValue()

        expect:
            AssetId.of(someValue) == AssetId.of(someValue)
    }

    def "two asset ids should not be equal when created from the different values"() {
        expect:
            AssetId.of(someAssetIdValue()) != AssetId.of(someAssetIdValue())
    }

}
