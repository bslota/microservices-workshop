package com.bartslota.availability.domain


import java.time.Instant
import java.time.InstantSource

/**
 * @author bslota on 19/03/2022
 */
class TimeProviderFixture {

    static TestTimeProvider timeProvider() {
        return new TestTimeProvider()
    }

    static class TestTimeProvider implements TimeProvider {

        private InstantSource instantSource = InstantSource.system()

        TestTimeProvider() {
        }

        @Override
        InstantSource get() {
            return instantSource;
        }

        TestTimeProvider thatIsFixedFor(Instant time) {
            instantSource = InstantSource.fixed(time)
            this
        }
    }

}
