package com.bartslota.availability.domain

import com.bartslota.availability.events.AssetActivated
import com.bartslota.availability.events.AssetActivationRejected
import com.bartslota.availability.events.AssetLockExpired
import com.bartslota.availability.events.AssetLockRejected
import com.bartslota.availability.events.AssetLocked
import com.bartslota.availability.events.AssetLockedIndefinitely
import com.bartslota.availability.events.AssetUnlocked
import com.bartslota.availability.events.AssetUnlockingIgnored
import com.bartslota.availability.events.AssetUnlockingRejected
import com.bartslota.availability.events.AssetWithdrawalRejected
import com.bartslota.availability.events.AssetWithdrawn
import io.vavr.control.Either
import spock.lang.Specification

import java.time.Duration
import java.time.Instant

import static com.bartslota.availability.domain.AssetAvailabilityFixture.someAsset
import static com.bartslota.availability.domain.AssetAvailabilityFixture.someNewAsset
import static com.bartslota.availability.domain.AssetIdFixture.someAssetId
import static com.bartslota.availability.domain.DurationFixture.someValidDuration
import static com.bartslota.availability.domain.OwnerIdFixture.someOwnerId
import static com.bartslota.availability.domain.TimeProviderFixture.timeProvider
import static java.time.temporal.ChronoUnit.MINUTES

/**
 * @author bslota on 19/03/2022
 */
class AssetAvailabilityTest extends Specification {

    def "should create asset availability with given id and time provider"() {
        expect:
            AssetAvailability.of(someAssetId(), timeProvider()) != null
    }

    def "should activate the new asset"() {
        given:
            AssetAvailability asset = someNewAsset()

        when:
            Either<AssetActivationRejected, AssetActivated> result = asset.activate()

        then:
            result.get() != null
    }

    def "should fail to activate the already activated asset"() {
        given:
            AssetAvailability asset = someAsset().thatIsActive().get()

        when:
            Either<AssetActivationRejected, AssetActivated> result = asset.activate()

        then:
            result.getLeft() != null
    }

    def "should fail to lock inactive asset"() {
        given:
            AssetAvailability asset = someNewAsset()
            OwnerId ownerId = someOwnerId()
            Duration duration = someValidDuration()

        when:
            Either<AssetLockRejected, AssetLocked> result = asset.lockFor(ownerId, duration)

        then:
            result.getLeft() != null
    }

    def "activated asset should be locked for given period"() {
        given:
            AssetAvailability asset = someAsset().thatIsActive().get()
            OwnerId ownerId = someOwnerId()
            Duration duration = someValidDuration()

        when:
            Either<AssetLockRejected, AssetLocked> result = asset.lockFor(ownerId, duration)

        then:
            result.get() != null
    }

    def "should extend the lock indefinitely when given owner has already locked the asset and the lock is active"() {
        given:
            OwnerId ownerId = someOwnerId()

        and:
            AssetAvailability asset = someAsset()
                    .thatIsActive()
                    .thatWasLockedByOwnerWith(ownerId).forSomeValidDuration()
                    .get()

        when:
            Either<AssetLockRejected, AssetLockedIndefinitely> result = asset.lockIndefinitelyFor(ownerId)

        then:
            result.get() != null
    }

    def "should fail to extend the lock indefinitely when given owner has already locked the asset but the lock is not active"() {
        given:
            OwnerId ownerId = someOwnerId()

        and:
            Instant currentTime = Instant.now()
            Instant lockTime = currentTime.minus(11, MINUTES)

        and:
            AssetAvailability asset = someAsset()
                    .thatIsActive()
                    .withClockSetTo(lockTime)
                    .thatWasLockedByOwnerWith(ownerId).forDurationOf(Duration.of(10, MINUTES))
                    .withClockSetTo(currentTime)
                    .get()

        when:
            Either<AssetLockRejected, AssetLockedIndefinitely> result = asset.lockIndefinitelyFor(ownerId)

        then:
            result.getLeft() != null
    }

    def "should fail to extend the lock when there is no lock on the asset"() {
        given:
            AssetAvailability asset = someAsset().thatIsActive().get()

        when:
            Either<AssetLockRejected, AssetLockedIndefinitely> result = asset.lockIndefinitelyFor(someOwnerId())

        then:
            result.getLeft() != null
    }

    def "should fail to extend the lock when the lock exists for other owner"() {
        given:
            AssetAvailability asset = someAsset()
                    .thatIsActive()
                    .thatWasLockedBySomeOwner().forSomeValidDuration()
                    .get()

        when:
            Either<AssetLockRejected, AssetLockedIndefinitely> result = asset.lockIndefinitelyFor(someOwnerId())

        then:
            result.getLeft() != null
    }

    def "should fail to lock already locked asset"() {
        given:
            AssetAvailability asset = someAsset()
                    .thatIsActive()
                    .thatWasLockedBySomeOwner().forSomeValidDuration()
                    .get()

        when:
            Either<AssetLockRejected, AssetLocked> result = asset.lockFor(someOwnerId(), someValidDuration())

        then:
            result.getLeft() != null
    }

    def "should unlock the asset locked by the same owner"() {
        given:
            OwnerId ownerId = someOwnerId()

        and:
            AssetAvailability asset = someAsset()
                    .thatIsActive()
                    .thatWasLockedByOwnerWith(ownerId).forSomeValidDuration()
                    .get()

        when:
            Either<AssetUnlockingRejected, AssetUnlocked> result = asset.unlockFor(ownerId)

        then:
            result.get() != null
    }

    def "should lock the asset after unlocking"() {
        given:
            AssetAvailability asset = someAsset()
                    .thatIsActive()
                    .thatWasLockedBySomeOwner().forSomeValidDuration()
                    .thenUnlocked()
                    .get()
        when:
            Either<AssetLockRejected, AssetLocked> result = asset.lockFor(someOwnerId(), someValidDuration())

        then:
            result.get() != null

    }

    def "should fail to unlock the asset locked by different owner"() {
        given:
            AssetAvailability asset = someAsset()
                    .thatIsActive()
                    .thatWasLockedBySomeOwner().forSomeValidDuration()
                    .get()

        when:
            Either<AssetUnlockingRejected, AssetUnlocked> result = asset.unlockFor(someOwnerId())

        then:
            result.getLeft() != null
    }

    def "should fail to unlock the unlocked asset"() {
        given:
            AssetAvailability asset = someAsset()
                    .thatIsActive()
                    .thatWasLockedBySomeOwner().forSomeValidDuration()
                    .thenUnlocked()
                    .get()

        when:
            Either<AssetUnlockingRejected, AssetUnlocked> result = asset.unlockFor(someOwnerId())

        then:
            result.getLeft() != null
    }

    def "should unlock the asset only when the lock is overdue"() {
        given:
            OwnerId ownerId = someOwnerId()

        and:
            Instant currentTime = Instant.now()
            Instant lockTime = currentTime.minus(11, MINUTES)

        and:
            AssetAvailability asset = someAsset()
                    .thatIsActive()
                    .withClockSetTo(lockTime)
                    .thatWasLockedByOwnerWith(ownerId).forDurationOf(Duration.of(10, MINUTES))
                    .withClockSetTo(currentTime)
                    .get()

        when:
            Either<AssetUnlockingIgnored, AssetLockExpired> result = asset.unlockIfOverdue()

        then:
            result.get() != null
    }

    def "should ignore overdue unlocking for the asset that was locked indefinitely"() {
        given:
            Instant currentTime = Instant.now()
            Instant lockTime = currentTime.minus(11, MINUTES)

        and:
            AssetAvailability asset = someAsset()
                    .thatIsActive()
                    .withClockSetTo(lockTime)
                    .thatWasLockedBySomeOwner().forDurationOf(Duration.of(10, MINUTES))
                    .thenLockedIndefinitely()
                    .withClockSetTo(currentTime)
                    .get()

        when:
            Either<AssetUnlockingIgnored, AssetLockExpired> result = asset.unlockIfOverdue()

        then:
            result.getLeft() != null
    }

    def "should withdraw inactive asset"() {
        given:
            AssetAvailability asset = someNewAsset()

        when:
            Either<AssetWithdrawalRejected, AssetWithdrawn> result = asset.withdraw()

        then:
            result.get()
    }

    def "should withdraw active asset"() {
        given:
            AssetAvailability asset = someAsset().thatIsActive().get()

        when:
            Either<AssetWithdrawalRejected, AssetWithdrawn> result = asset.withdraw()

        then:
            result.get()
    }

    def "should withdraw active asset that was unlocked"() {
        given:
            AssetAvailability asset = someAsset()
                    .thatIsActive()
                    .thatWasLockedBySomeOwner().forSomeValidDuration()
                    .thenUnlocked()
                    .get()

        when:
            Either<AssetWithdrawalRejected, AssetWithdrawn> result = asset.withdraw()

        then:
            result.get()
    }

    def "should fail to withdraw the locked asset"() {
        given:
            AssetAvailability asset = someAsset()
                    .thatIsActive()
                    .thatWasLockedBySomeOwner().forSomeValidDuration()
                    .get()
        when:
            Either<AssetWithdrawalRejected, AssetWithdrawn> result = asset.withdraw()

        then:
            result.getLeft()
    }

    def "should fail to activate the withdrawn asset"() {
        given:
            AssetAvailability asset = someAsset()
                    .thatIsWithdrawn()
                    .get()
        when:
            Either<AssetActivationRejected, AssetActivated> result = asset.activate()

        then:
            result.getLeft()
    }

    def "two assets with the same ids should be equal regardless state"() {
        given:
            AssetId assetId = someAssetId()

        expect:
            someAsset().with(assetId).get() == someAsset().with(assetId).thatIsActive().get()
    }

    def "two assets with different ids should not be equal"() {
        expect:
            someAsset().with(someAssetId()).get() != someAsset().with(someAssetId()).get()
    }
}
