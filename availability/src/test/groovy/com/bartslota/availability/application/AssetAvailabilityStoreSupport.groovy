package com.bartslota.availability.application

import com.bartslota.availability.domain.AssetAvailability
import com.bartslota.availability.domain.AssetAvailabilityRepository
import com.bartslota.availability.domain.AssetId
import com.bartslota.availability.domain.OwnerId
import com.bartslota.availability.domain.OwnerIdFixture
import groovy.transform.SelfType

import java.time.Duration

import static com.bartslota.availability.domain.AssetAvailabilityFixture.someAsset
import static com.bartslota.availability.domain.AssetAvailabilityFixture.someNewAsset
import static com.bartslota.availability.domain.OwnerIdFixture.someOwnerId
import static java.time.temporal.ChronoUnit.MINUTES

/**
 * @author bslota on 25/11/2021
 */
@SelfType(TimeSupport)
trait AssetAvailabilityStoreSupport {

    abstract AssetAvailabilityRepository repository()

    AssetAvailability existingAsset() {
        AssetAvailability asset = someAsset().withClockSetTo(timeProvider().instant()).get()
        repository().save(asset)
        asset
    }

    AssetAvailability activatedAsset() {
        AssetAvailability asset = someAsset().thatIsActive().withClockSetTo(timeProvider().instant()).get()
        repository().save(asset)
        asset
    }

    AssetAvailability assetLockedBy(OwnerId ownerId) {
        AssetAvailability asset = someAsset().thatIsActive().withClockSetTo(timeProvider().instant()).thatWasLockedByOwnerWith(ownerId).forSomeValidDuration().get()
        repository().save(asset)
        asset
    }

    AssetAvailability lockedAsset() {
        AssetAvailability asset = someAsset().thatIsActive().withClockSetTo(timeProvider().instant()).thatWasLockedBySomeOwner().forSomeValidDuration().get()
        repository().save(asset)
        asset
    }

    AssetAvailability assetWithOverdueLock(OwnerId ownerId = someOwnerId()) {
        AssetAvailability asset = someAsset()
                .thatIsActive()
                .withClockSetTo(timeProvider().instant().minus(11, MINUTES))
                .thatWasLockedByOwnerWith(ownerId).forDurationOf(Duration.of(10, MINUTES))
                .withClockSetTo(timeProvider().instant())
                .get()
        repository().save(asset)
        asset
    }

    boolean thereIsAWithdrawnAssetWith(AssetId id) {
        repository().findBy(id).flatMap { it.currentLock() }.filter { it instanceof AssetAvailability.WithdrawalLock }.isPresent()
    }

    boolean thereIsALockedAssetWith(AssetId assetId, OwnerId ownerId) {
        repository().findBy(assetId).flatMap { it.currentLock() }.filter { it instanceof AssetAvailability.OwnerLock && it.ownerId() == ownerId }.isPresent()
    }

    boolean thereIsAnIndefinitelyLockedAssetWith(AssetId assetId, OwnerId ownerId) {
        repository().findBy(assetId).flatMap { it.currentLock() }.filter { it instanceof AssetAvailability.IndefiniteOwnerLock && it.ownerId() == ownerId }.isPresent()
    }

    boolean assetIsRegisteredWith(AssetId id) {
        repository().findBy(id).flatMap { it.currentLock() }.filter { it instanceof AssetAvailability.MaintenanceLock }.isPresent()
    }

    boolean assetIsActivatedWith(AssetId id) {
        repository().findBy(id).filter { !it.currentLock() }.isPresent()
    }

    boolean thereIsAnUnlockedAssetWith(AssetId assetId) {
        repository().findBy(assetId).filter { it.currentLock().isEmpty() }.isPresent()
    }
}