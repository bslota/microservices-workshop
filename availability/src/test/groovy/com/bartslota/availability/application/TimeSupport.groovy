package com.bartslota.availability.application

import com.bartslota.availability.domain.TimeProvider
import com.bartslota.availability.domain.TimeProviderFixture

import java.time.Instant
import java.time.InstantSource

import static java.time.Instant.now

/**
 * @author bslota on 20/03/2022
 */
trait TimeSupport {

    abstract TimeProvider timeProvider()
}